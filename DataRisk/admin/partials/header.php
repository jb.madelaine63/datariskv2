<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/header.css"/>
    <link rel="stylesheet" href="/admin/css/fondAdmin.css" />
    <title>DataRisk</title>
</head>
<body>
<script>const dataFromDB = 'string';</script>
<script src="/assets/js/scripts.js"></script>
<header>
    <nav class="navbar navbar-expand-lg navbar-light #DB642A">
        <div class="container-fluid" id="logoTitle">
            <img class="logo" src="/assets/images/carte.jpg" alt="">
            <a class="navbar-brand" id="title" href="../users/index.php">Datarisk</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" id="btn1" aria-current="page" href="../users/index.php">Carte</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Profil
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="/admin/partials/compte.php">Compte</a></li>
                            <li><a class="dropdown-item" href="/admin/partials/forum.php">Forum</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="/index.php">Se déconnecter</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="d-flex" id="btn1">
                    <input class="form-control me-2"  type="search" placeholder="Rechercher" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Rechercher</button>
                </form>
            </div>
        </div>
    </nav>
</header>
