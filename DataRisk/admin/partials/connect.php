<?php
require_once '../../core/functions.php';
getHeaderCo()
?>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/admin/css/fondAdmin.css" />
    <title>Connection</title>
</head>
<main>
    <div class="container">
        <div class="row justify-content-center">
            <h2>Connection</h2>
            <form method="post">
                <div class="mb-3">
                    <label for="login" class="form-label">Email</label>
                    <input
                        type="email"
                        name="login"
                        class="form-control"
                        id="login"
                        aria-describedby="emailHelp"
                        required
                    >
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Mot de passe</label>
                    <input type="password" name="password" class="form-control" id="password" required>
                </div>
            </form>
            <div class="bouton2">
                <a href="../users/index.php">Se connecter</a>
            </div>
        </div>
    </div>
</main>

<?php
getFooter();
?>
