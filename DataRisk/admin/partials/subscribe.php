<?php
require_once '../../core/functions.php';
getHeaderCo()
?>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/admin/css/fondAdmin.css" />
    <title>Inscription</title>
</head>
<main>
    <div class="container mt-5 mb-5">
        <div class="row justify-content-center">
            <h2>Inscription</h2>
            <form method="post">
                <div class="mb-3">
                    <label for="prenom" class="form-label">Prénom</label>
                    <input
                        type="text"
                        name="prenom"
                        class="form-control"
                        id="prenom"
                        required
                    >
                </div>
                <div class="mb-3">
                    <label for="nom" class="form-label">Nom</label>
                    <input
                        type="text"
                        name="nom"
                        class="form-control"
                        id="nom"
                        required
                    >
                </div>
                <div class="mb-3">
                    <label for="adresse" class="form-label">Adresse</label>
                    <input
                        type="text"
                        name="adresse"
                        class="form-control"
                        id="adresse"
                        required
                    >
                </div>
                <div class="mb-3">
                    <label for="login" class="form-label">Email</label>
                    <input
                        type="email"
                        name="login"
                        class="form-control"
                        id="login"
                        aria-describedby="emailHelp"
                        required
                    >
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Mot de passe</label>
                    <input type="password" name="password" class="form-control" id="password" required>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Confirmer mot de passe</label>
                    <input type="password" name="password" class="form-control" id="password" required>
                </div>
            </form>
            <div class="bouton2">
                <a href="../../index.php">S'inscrire</a>
            </div>
        </div>
    </div>
</main>
<?php
getFooterAdmin();
