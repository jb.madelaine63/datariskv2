<?php

?>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/main.js"></script>
<link rel="stylesheet" href="/assets/css/footer.css">
<footer class="DB642A text-center text-lg-start" >
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row">
            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">Sites d'informations</h5>

                <ul class="list-unstyled">
                    <li>
                        <img class="footer" src="/assets/images/eau.jpg" alt="">
                        <a href="https://www.data.gouv.fr/fr/datasets/resultats-du-controle-sanitaire-de-leau-distribuee-commune-par-commune/" class="text-dark">Qualité de l'eau</a>
                    </li>
                    <li>
                        <img class="footer" src="/assets/images/eau.jpg" alt="">
                        <a href="https://www.gouvernement.fr/risques/seisme" class="text-dark">Risques sismiques</a>
                    </li>
                    <li>
                        <img class="footer" src="/assets/images/eau.jpg" alt="">
                        <a href="https://www.data.gouv.fr/fr/reuses/cartes-des-installations-classees-seveso-haut/" class="text-dark">Sites seveso</a>
                    </li>
                    <li>
                        <img class="footer" src="/assets/images/eau.jpg" alt="">
                        <a href="https://atmo-france.org/lindice-atmo/" class="text-dark">Indice ATMO</a>
                    </li>
                    <li>
                        <img class="footer" src="/assets/images/eau.jpg" alt="">
                        <a href="https://www.service-public.fr/particuliers/vosdroits/F3076" class="text-dark">Catastrophes naturelles</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">Navigation</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="../partials/subscribe.php" class="text-dark">S'inscrire</a>
                    </li>
                    <li>
                        <a href="connect.php" class="text-dark">Se connecter</a>
                    </li>
                </ul>
            </div>
            <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
        © 2020 Copyright:
        <a class="text-dark" href="../users/index.php">DataRisk</a>
    </div>
    <!-- Copyright -->
</footer>
</body>
</html>