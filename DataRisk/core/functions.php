<?php
function getHeader()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/header.php';
}
function getHeaderCo()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/headerCo.php';
}
function getHeaderAdmin()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/admin/partials/header.php';
}
function getHeaderUsers()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/admin/users/header.php';
}
function getFooter()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/partials/footer.php';
}
function getFooterAdmin()
{
    include $_SERVER['DOCUMENT_ROOT'] . '/admin/partials/footer.php';
}
