<?php
require_once 'core/functions.php';
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/header.css" />
    <link rel="stylesheet" href="/assets/css/main.css" />
    <link rel="stylesheet" href="/assets/css/footer.css" />
    <script src="/assets/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.4.1/dist/MarkerCluster.Default.css">
    <title>DataRisk</title>
</head>
<body>
<?php
getHeader();
?>
<main>
    <div class="col-3" >
    <div class="accordion accordion-flush" id="accordionFlushExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingOne">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    Risques industriels
                </button>
            </h2>
            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                <ul class="accordion-body">
                    <li>Sites Seveso</li>
                </ul>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                    Catastrophes naturelles
                </button>
            </h2>
            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                <ul class="accordion-body">
                    <li>Risques sismiques</li>
                    <li>Risques d'inondations</li>
                </ul>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                    Risques sanitaires
                </button>
            </h2>
            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                <ul class="accordion-body">
                    <li>Qualité de l'air</li>
                    <li>Qualité de l'eau</li>
                </ul>
            </div>
        </div>
    </div>
    </div>
    <div class="col" id="maCarte"></div>


    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src="https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"></script>
    <script>
        var villes = {
            "Paris": { "lat": 48.852969, "lon": 2.349903 },
            "Brest": { "lat": 48.383, "lon": -4.500 },
            "Quimper": { "lat": 48.000, "lon": -4.100 },
            "Bayonne": { "lat": 43.500, "lon": -1.467 }
        };
        var tableauMarqueurs = [];

        // On initialise la carte
        var carte = L.map('maCarte').setView([48.852969, 2.349903], 13);

        // On charge les "tuiles"
        L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            // Il est toujours bien de laisser le lien vers la source des données
            attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
            minZoom: 1,
            maxZoom: 20
        }).addTo(carte);
    </script>
    <div class="col-3" id="col-info">
        <div>
            <span>Météo</span>
        </div>
        <div id="widget">
            <iframe id="widget_autocomplete_preview"  width="300" height="150" frameborder="0" src="https://meteofrance.com/widget/prevision/631130##DB642A"> </iframe>
        </div>
        <div class="incidents">
            <span>Derniers incidents</span>
                <p>A75, de Paris vers Clermont (sens nord-sud)
                    au PR 83+182
                    à Voreppe
                    situé sur la bretelle de sortie
                    1 véhicule(s) léger(s) A48 PK 83,9 N Bretelle sortie Accident - Etat : confirmé Voie(s) concernée(s) : VR . 1 véhicule impliqué.
                </p>
                <p>A75, de Paris vers Clermont (sens nord-sud)
                    au PR 83+182
                    à Voreppe
                    situé sur la bretelle de sortie
                    1 véhicule(s) léger(s) A48 PK 83,9 N Bretelle sortie Accident - Etat : confirmé Voie(s) concernée(s) : VR . 1 véhicule impliqué.
                </p>
                <p>A75, de Paris vers Clermont (sens nord-sud)
                    au PR 83+182
                    à Voreppe
                    situé sur la bretelle de sortie
                    1 véhicule(s) léger(s) A48 PK 83,9 N Bretelle sortie Accident - Etat : confirmé Voie(s) concernée(s) : VR . 1 véhicule impliqué.
                </p>
        </div>
    </div>
</main>

<?php
getFooter();
?>
</body>
</html>
</html>


