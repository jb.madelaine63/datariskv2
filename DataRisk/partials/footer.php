
<footer class="DB642A text-center text-lg-start" >
    <div class="container">
        <div class="col-lg-3">
            <h5 class="text-uppercase">Sites d'informations</h5>
            <ul class="list-unstyled">
                <li>
                    <img class="footer" src="/assets/images/eau.jpg" alt="">
                    <a href="https://www.data.gouv.fr/fr/datasets/resultats-du-controle-sanitaire-de-leau-distribuee-commune-par-commune/" class="text-white">Qualité de l'eau</a>
                </li>
                <li>
                    <img class="footer" src="/assets/images/picto-seisme.jpg" alt="">
                    <a href="https://www.gouvernement.fr/risques/seisme" class="text-white">Risques sismiques</a>
                </li>
                <li>
                    <img class="footer" src="/assets/images/Seveso.jpg" alt="">
                    <a href="https://www.data.gouv.fr/fr/reuses/cartes-des-installations-classees-seveso-haut/" class="text-white">Sites seveso</a>
                </li>
                <li>
                    <img class="footer" src="/assets/images/atmo.jpeg" alt="">
                    <a href="https://atmo-france.org/lindice-atmo/" class="text-white">Indice ATMO</a>
                </li>
                <li>
                    <img class="footer" src="/assets/images/Catastrophe_Naturelle.jpg" alt="">
                    <a href="https://www.service-public.fr/particuliers/vosdroits/F3076" class="text-white">Catastrophes naturelles</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3">
            <h5 class="text-uppercase">Navigation</h5>
            <ul class="list-unstyled">
                <li>
                    <a href="../admin/partials/subscribe.php" class="text-white">S'inscrire</a>
                </li>
                <li>
                    <a href="../admin/partials/connect.php" class="text-white">Se connecter</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
        © 2020 Copyright:
        <a class="text-white" href="../index.php">DataRisk</a>
    </div>
</footer>
